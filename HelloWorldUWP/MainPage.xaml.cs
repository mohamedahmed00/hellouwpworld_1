﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace HelloWorldUWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        // field variables:
        BitmapImage _avatarImage;
        public MainPage()
        {
            this.InitializeComponent();
            // create an image object for the avatar 
            // use new to make an object 
            _avatarImage = new BitmapImage(new Uri("ms-appex:///Assets/giphy.gif"));
        }

        private void onShowAvatar(object sender, RoutedEventArgs e)
        {
            // set the source of the image control to the avatar image
            _imgAvatar.Source = _avatarImage;
            // show the image control 

            // _imgAvatar.Visibility = Visibility.Visible;

            // change the caption of the button
            _btnShowAvatar.Content = "tap on the image ";
        }

      
        private void onImageAvatarTap(object sender, TappedRoutedEventArgs e)
        {
            
        }
        /// <summary>
        /// plays or pauses the animation of the gif file
        /// </summary>
        /// <param name="sender">the hello_imgAvatar control</param>
        /// <param name="e">not used</param>
        private void _onImageAvatarTap(object sender, TappedRoutedEventArgs e)
        {
            // obtain the image object show the image control
            _avatarImage = _imgAvatar.Source as BitmapImage;

           // check if the image is animated
           if (_avatarImage.IsAnimatedBitmap)
            {
           
                // check if the image is playing
                if (_avatarImage.IsPlaying)
                {
                    // if the image is playing, stop it
                    
                    _avatarImage.Stop();

                }
                else
                {
                    // if the image is not playing, play it 
                    _avatarImage.Play();

                }
                
                
                

                

            }



        }
    }
}
